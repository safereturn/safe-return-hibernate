package safe.ret.hibernate.enums;

public enum RelationType {

	FAMILY(0), 
	FRIEND(1), 
	OTHER(2);
	
	private int value;
  
	private RelationType(int value) {
      this.value = value;
	}

	public int getValue() {
      return value;
	}
	
}
