package safe.ret.hibernate.enums;

public enum UserType {

	CONTACT(0), 
	TRACKED(1); 
	
	
	private int value;
  
	private UserType(int value) {
      this.value = value;
	}

	public int getValue() {
      return value;
	}
	
}
