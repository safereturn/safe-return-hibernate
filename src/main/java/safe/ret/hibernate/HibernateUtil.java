package safe.ret.hibernate;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateUtil {

	static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);

	private static final EntityManagerFactory factory;

	static {
		try {
			factory = Persistence.createEntityManagerFactory("postgres");
			logger.debug("********** EntityManagerFactory successfully created. **********");
			// System.out.println();
		} catch (Throwable ex) {

			logger.debug("********** EntityManagerFactory creation failed. **********", ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static EntityManagerFactory getEntityManagerFactory() {
		return factory;
	}

}
