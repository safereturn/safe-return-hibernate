package safe.ret.hibernate.utils;

import java.util.List;

public class Utils {

	public static boolean ListNullOrEmpty(List<?> list) {
		
		return list == null || list.isEmpty();
	}
	

	/**
	 * Calculates distance between two Locations using
	 * Haversine formula {@link https://en.wikipedia.org/wiki/Haversine_formula}
	 * source {@link http://stackoverflow.com/questions/120283/how-can-i-measure-distance-and-create-a-bounding-box-based-on-two-latitudelongi}
	 * 
	 * Returns distance in km
	 * 
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return
	 */
	
	public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
	    //double earthRadius = 3958.75; // miles 
	    double earthRadius = 6371.0; //kilometers
	    double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lng2-lng1);
	    double sindLat = Math.sin(dLat / 2);
	    double sindLng = Math.sin(dLng / 2);
	    double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
	            * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double dist = earthRadius * c;

	    return dist;
	}
	
	
}
