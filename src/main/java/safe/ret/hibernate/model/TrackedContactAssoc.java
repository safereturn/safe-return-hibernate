package safe.ret.hibernate.model;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "tracked_contact_assoc")
@AssociationOverrides({
	@AssociationOverride(name = "id.trackedUser",joinColumns = @JoinColumn(name = "tracked_user_id")),
	@AssociationOverride(name = "id.contactPerson",	joinColumns = @JoinColumn(name = "contact_person_id")) })

public class TrackedContactAssoc {
 
	@EmbeddedId
    private TrackedContactAssocId id;
	
	private String contactDisplayName;
	
	
	public TrackedContactAssoc() {
		super();
	}
	public TrackedContactAssoc(TrackedUser trackedUser,ContactPerson contactPerson, String contactDisplayName) {
		super();
		this.id = new TrackedContactAssocId(trackedUser, contactPerson);
		this.contactDisplayName = contactDisplayName;
	}
	public TrackedContactAssocId getId() {
		return id;
	}
	public void setId(TrackedContactAssocId id) {
		this.id = id;
	}
	
	public TrackedUser getTrackedUser() {
		return this.id.getTrackedUser();
	}
	public void setTrackedUser(TrackedUser trackedUser) {
		this.getId().setTrackedUser(trackedUser);
	}
	public ContactPerson getContactPerson() {
		return this.getId().getContactPerson();
	}
	public void setContactPerson(ContactPerson contactPerson) {
		this.getId().setContactPerson(contactPerson);
	}
	public String getContactDisplayName() {
		return contactDisplayName;
	}
	public void setContactDisplayName(String contactDisplayName) {
		this.contactDisplayName = contactDisplayName;
	}
		
}
