package safe.ret.hibernate.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries({

		@NamedQuery(name = TrackedUser.getAllTrackedUsers, query = "SELECT tu FROM TrackedUser tu"),
		@NamedQuery(name = TrackedUser.getTrackedUserByIdPass, query = "SELECT tu FROM TrackedUser tu WHERE tu.id=:id AND password=:pass"),
		@NamedQuery(name = TrackedUser.updateToken, query = "UPDATE TrackedUser t set t.registrationToken=:token WHERE  t.id=:userId")

})
@Table(name = "tracked_user")
@SequenceGenerator(name = "safe_return_user_seq_id", sequenceName = "tracked_user_seq_id", allocationSize = 1)
public class TrackedUser extends SafeReturnUser {

	private static final long serialVersionUID = 1L;

	public static final String getAllTrackedUsers = "TrackedUser.getAllTrackedUsers";
	public static final String getTrackedUserByIdPass = "TrackedUser.getTrackedUserByIdPass";
 	public static final String updateToken = "TrackedUser.updateToken";

	public TrackedUser() {

	}

	@Column(name = "within_radius")
	private boolean withinRadius = true;

	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id.trackedUser", cascade = CascadeType.ALL)
	private List<TrackedContactAssoc> trackedContactAssoc;

	 
	public List<TrackedContactAssoc> getTrackedContactAssoc() {
		return trackedContactAssoc;
	}

	public void setTrackedContactAssoc(List<TrackedContactAssoc> trackedContactAssoc) {
		this.trackedContactAssoc = trackedContactAssoc;
	}

	public boolean isWithinRadius() {
		return withinRadius;
	}

	public void setWithinRadius(boolean withinRadius) {
		this.withinRadius = withinRadius;
	}

}
