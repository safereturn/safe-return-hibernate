package safe.ret.hibernate.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

@Embeddable
public class TrackedContactAssocId implements java.io.Serializable {
 
	private static final long serialVersionUID = 1L;
	@ManyToOne
	private TrackedUser trackedUser;
	@ManyToOne
	private ContactPerson contactPerson;

	public TrackedContactAssocId() {
	}

	public TrackedContactAssocId(TrackedUser trackedUser, ContactPerson contactPerson) {
		super();
		this.trackedUser = trackedUser;
		this.contactPerson = contactPerson;
	}

	public TrackedUser getTrackedUser() {
		return trackedUser;
	}

	public void setTrackedUser(TrackedUser trackedUser) {
		this.trackedUser = trackedUser;
	}

	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contactPerson == null) ? 0 : contactPerson.hashCode());
		result = prime * result + ((trackedUser == null) ? 0 : trackedUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		TrackedContactAssoc other = (TrackedContactAssoc) obj;
		return this.contactPerson.id.equals(other.getContactPerson().getId())
				&& this.trackedUser.id.equals(other.getTrackedUser().getId());
	}

}