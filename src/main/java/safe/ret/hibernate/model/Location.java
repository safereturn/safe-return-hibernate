package safe.ret.hibernate.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@NamedQueries({

		@NamedQuery(name = Location.getAllLocationsByTrackedUser, query = "SELECT l FROM Location l WHERE l.trackedUser.id = :trackedUserID"),

		@NamedQuery(name = Location.getHomeLocationOfTrackedUser, query = "SELECT l FROM Location l WHERE l.trackedUser.id = :trackedUserID"
				+ " AND l.home = TRUE"),
		
		@NamedQuery(name = Location.getLocationsByTrackedUserAndIsHome, query = "SELECT l FROM Location l WHERE l.trackedUser.id = :trackedUserID"
				+ " AND l.home = :home"),

		@NamedQuery(name = Location.getLastKnownLocation, query = "SELECT l FROM Location l WHERE l.trackedUser.id = :trackedUserID AND l.home=FALSE AND l.date = "
				+ "(SELECT MAX(ll.date) from Location ll where ll.trackedUser.id = :trackedUserID AND ll.home=FALSE)")

})
@Table(name = "location")

public class Location implements Serializable {

	private static final long serialVersionUID = 1L;

	public static final String getAllLocationsByTrackedUser = "Location.getAllLocationsByTrackedUser";
	public static final String getHomeLocationOfTrackedUser = "Location.getHomeLocationOfTrackedUser";

	public static final String getLastKnownLocation = "Location.getLastKnownLocation";

	public static final String getLocationsByTrackedUserAndIsHome  = "Location.getLocationsByTrackedUserAndIsHome";

	@Id
	@SequenceGenerator(name = "location_seq_id", sequenceName = "location_seq_id", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "location_seq_id")
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "latitude")
	private Double latitude;

	@Column(name = "longitude")
	private Double longitude;
 
	@Column(name = "date")
	private Date date;

	@ManyToOne
	@JoinColumn(name = "tracked_user_fk", foreignKey =  @ForeignKey(name = "FK_USER", value=ConstraintMode.CONSTRAINT)) 
 	private TrackedUser trackedUser;

	@Column(name = "home")
	private boolean home;

	@Column(name = "safe")
	private boolean safe;

	public Location() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TrackedUser getTrackedUser() {
		return trackedUser;
	}

	public void setTrackedUser(TrackedUser trackedUser) {
		this.trackedUser = trackedUser;
	}

	public boolean isHome() {
		return home;
	}

	public void setHome(boolean home) {
		this.home = home;
	}

	public boolean isSafe() {
		return safe;
	}

	public void setSafe(boolean safe) {
		this.safe = safe;
	}
	

}
