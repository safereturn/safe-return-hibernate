package safe.ret.hibernate.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import safe.ret.hibernate.enums.RelationType;

@Entity
@NamedQueries({
		@NamedQuery(name = ContactPerson.updateToken, query = "UPDATE ContactPerson t SET t.registrationToken=:token WHERE t.id=:userId"),
		@NamedQuery(name = ContactPerson.getByMobile, query = "SELECT cp FROM ContactPerson cp WHERE cp.mobile=:mobile"),
		@NamedQuery(name = ContactPerson.getContactsByTrackedUser, query = "SELECT cp FROM ContactPerson cp WHERE cp.trackedUser=:trackedUser")
})
@Table(name = "contact_person")
@SequenceGenerator(name = "safe_return_user_seq_id", sequenceName = "contact_person_seq_id", allocationSize = 1)
public class ContactPerson extends SafeReturnUser {

	private static final long serialVersionUID = 1L;

 	public static final String updateToken = "ContactPerson.updateToken";
	public static final String getByMobile = "ContactPerson.getByMobile";
	public static final String getContactsByTrackedUser = "ContactPerson.getContactsByTrackedUser";
	 
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id.contactPerson")
	private List<TrackedContactAssoc> trackedContactAssoc;
 
	public ContactPerson() {

	}

	@Enumerated(EnumType.STRING)
	@Column(name = "relation_type_enum")
	private RelationType relationType;
	
	@ManyToOne
	@JoinColumn(name = "tracked_user_fk")
	private TrackedUser trackedUser;

	public RelationType getRelationType() {
		return relationType;
	}

	public void setRelationType(RelationType relationType) {
		this.relationType = relationType;
	}

	public TrackedUser getTrackedUser() {
		return trackedUser;
	}

	public void setTrackedUser(TrackedUser trackedUser) {
		this.trackedUser = trackedUser;
	}
	
	

}
