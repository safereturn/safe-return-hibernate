package safe.ret.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

@Entity
@NamedQueries({
	  @NamedQuery(name= Options.getOptionsByUserid,
			  query="SELECT o FROM Options o  WHERE o.trackedUser.id = :userid") 
	})
public class Options {

	public static final String getOptionsByUserid = "Options.getOptionsByUserid";
	
	@Id
	@SequenceGenerator(name="options_seq_id",sequenceName="options_seq_id",allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="options_seq_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "tracked_user_fk")
	private TrackedUser trackedUser;
	
	@Column(name = "allowed_distance")
	private double allowedDistance;
	
	@Column(name = "time_interval")
	private int timeInterval;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TrackedUser getTrackedUser() {
		return trackedUser;
	}

	public void setTrackedUser(TrackedUser trackedUser) {
		this.trackedUser = trackedUser;
	}

	public double getAllowedDistance() {
		return allowedDistance;
	}

	public void setAllowedDistance(double allowedDistance) {
		this.allowedDistance = allowedDistance;
	}

	public int getTimeInterval() {
		return timeInterval;
	}

	public void setTimeInterval(int timeInterval) {
		this.timeInterval = timeInterval;
	}

}
