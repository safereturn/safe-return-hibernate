//package safe.ret.hibernate.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToOne;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@NamedQueries({
//	  @NamedQuery(name="TrackedUserInfo.getTrackedUserInfoByTrackedUser",
//			  query="SELECT ui FROM TrackedUserInfo ui"
//	              + " WHERE ui.trackedUser.username = :trackedUserUsername"),
//
//	})
//@Table(name = "tracked_user_info")
//public class TrackedUserInfo implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//
//	public static final String getTrackedUserInfoByTrackedUser = "TrackedUserInfo.getTrackedUserInfoByTrackedUser";
//	
//	@Id
//	@SequenceGenerator(name="tracked_user_info_seq_id",
//    	sequenceName="tracked_user_info_seq_id",allocationSize=1)
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="tracked_user_info_seq_id")
//	@Column(name = "id", updatable=false)
//	private Long id;
//
//	@Column(name = "name")
//	private String name;
//	
//	@Column(name = "surname")
//	private String surname;
//	
//	@Column(name = "age")
//	private String age;
//	
//	@Column(name = "condition")
//	private String condition;
//	
//	@Column(name = "mobile_number")
//	private String mobileNumber;
//	
//	@OneToOne
//	@JoinColumn(name = "tracked_user_fk")
//	private TrackedUser trackedUser;
//	
//	
//	public TrackedUserInfo() {
//		
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public String getSurname() {
//		return surname;
//	}
//
//	public void setSurname(String surname) {
//		this.surname = surname;
//	}
//
//	public String getAge() {
//		return age;
//	}
//
//	public void setAge(String age) {
//		this.age = age;
//	}
//
//	public String getCondition() {
//		return condition;
//	}
//
//	public void setCondition(String condition) {
//		this.condition = condition;
//	}
//
//	public String getMobileNumber() {
//		return mobileNumber;
//	}
//
//	public void setMobileNumber(String mobileNumber) {
//		this.mobileNumber = mobileNumber;
//	}
//
//	public TrackedUser getTrackedUser() {
//		return trackedUser;
//	}
//
//	public void setTrackedUser(TrackedUser trackedUser) {
//		this.trackedUser = trackedUser;
//	}
//
//	public Long getId() {
//		return id;
//	}
//	
//	
//}
