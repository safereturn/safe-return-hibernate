package safe.ret.hibernate;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class HibernateListener implements ServletContextListener {  
  
    public void contextInitialized(ServletContextEvent event) {  
        HibernateUtil.getEntityManagerFactory();      
    }  
  
    public void contextDestroyed(ServletContextEvent event) {  
        HibernateUtil.getEntityManagerFactory().close(); 
    }  
}  