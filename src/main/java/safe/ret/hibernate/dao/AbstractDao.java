package safe.ret.hibernate.dao;

import javax.persistence.EntityManager;

import safe.ret.hibernate.HibernateUtil;

public class AbstractDao {

	
	public <T> T insert(T obj, Class<T> type) {

		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();

		em.persist(obj);

		em.getTransaction().commit();
		em.close();

		return obj;
	}
	
	public<T> T saveOrUpdate(T obj,Class<T> type,Object id){
		T saved = null;
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
	
		if(id != null) {
			T fetchedObj = em.find(type, id);
			if(fetchedObj != null ){
				saved = em.merge(obj);
 			}else{
 			}
		}else{
			em.persist(obj);
			saved = obj;
 		}
		em.getTransaction( ).commit( );
		em.close();
	
		return saved;
	}
}
