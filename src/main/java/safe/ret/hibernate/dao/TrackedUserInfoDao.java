//package safe.ret.hibernate.dao;
//
//import javax.persistence.EntityManager;
//import javax.persistence.Query;
//
//import safe.ret.hibernate.HibernateUtil;
//import safe.ret.hibernate.model.TrackedUserInfo;
//
//public class TrackedUserInfoDao {
//
//	public TrackedUserInfo getTrackedUserInfoByTrackedUser(Long trackedUserID) {
//		
//		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
//		Query query = em.createNamedQuery(TrackedUserInfo.getTrackedUserInfoByTrackedUser);
//		query.setParameter("trackedUserID", trackedUserID);
//	
//		TrackedUserInfo info = (TrackedUserInfo) query.getSingleResult();
//		em.close();
//		return info;
//	}
//	
//	public boolean saveOrUpdateTrackedUserInfo(TrackedUserInfo trackedUserInfo) {
//		
//		boolean result = false;
//		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
//		em.getTransaction().begin();
//	
//		if(trackedUserInfo.getId() != null) {
//			TrackedUserInfo info = em.find(TrackedUserInfo.class, trackedUserInfo.getId());
//			if(info != null ){
//				em.merge(trackedUserInfo);
//				result = true;
//			}else{
//				result = false;
//			}
//		}else{
//			em.persist(trackedUserInfo);
//			result = true;
//		}
//		em.getTransaction( ).commit( );
//		em.close();
//	
//		return result;
//	}
//	
//	public boolean deleteTrackedUserInfo(Long trackedUserInfoID) {
//
//		boolean result = false;
//		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
//		em.getTransaction().begin();
//		
//		TrackedUserInfo info = em.find(TrackedUserInfo.class, trackedUserInfoID);
//		if(info != null ){
//			em.remove(info);
//			result = true;
//		}
//		em.getTransaction( ).commit( );
//		em.close();
//
//		return result;
//	}
//	
//}
