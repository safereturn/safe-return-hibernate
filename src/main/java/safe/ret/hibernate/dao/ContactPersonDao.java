package safe.ret.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import safe.ret.hibernate.HibernateUtil;
import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.Location;
import safe.ret.hibernate.model.TrackedUser;

public class ContactPersonDao extends AbstractDao {


	public ContactPerson getContactPersonByID(Long id) {

		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		ContactPerson contactPerson = em.find(ContactPerson.class, id);
		em.close();
		return contactPerson;
	}
	
	public List<ContactPerson> getContactsByTrackedUser(TrackedUser trackedUser) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(ContactPerson.getContactsByTrackedUser);
		query.setParameter("trackedUser", trackedUser);
		List<ContactPerson> contacts = query.getResultList();
		em.close();
		return contacts;
		
	}

	public ContactPerson saveOrUpdateContactPerson(ContactPerson contactPerson) {

		return super.saveOrUpdate(contactPerson, ContactPerson.class, contactPerson.getId());
	}

	public boolean deleteContactPerson(Long contactPersonID) {

		boolean result = false;
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();

		ContactPerson person = em.find(ContactPerson.class, contactPersonID);
		if (person != null) {
			em.remove(person);
			result = true;
		}
		em.getTransaction().commit();
		em.close();

		return result;
	}

	public int updateToken(Long userId, String token) {
		int rows;
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();

		Query query = em.createNamedQuery(ContactPerson.updateToken);
		query.setParameter("userId", userId);
		query.setParameter("token", token);
		rows = query.executeUpdate();
		em.getTransaction().commit();
		em.close();
		return rows;
	}

	public ContactPerson findByMobile(String mobile) {

		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();

		Query query = em.createNamedQuery(ContactPerson.getByMobile);
		query.setParameter("mobile", mobile);
		try {
			ContactPerson contactPerson = (ContactPerson) query.getSingleResult();
			return contactPerson;
		} catch (NoResultException nre) {
			return null;
		} finally {
			em.close();
		}
	}

	 

 

}
