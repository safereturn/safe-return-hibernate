package safe.ret.hibernate.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import safe.ret.hibernate.HibernateUtil;
import safe.ret.hibernate.model.Options;

public class OptionsDao extends AbstractDao{
	
	
	public Options saveOrUpdateOptions(Options options) {
		
		return super.saveOrUpdate(options, Options.class, options.getId());
	}
	
	
	public Options getOptionsByUserid(Long userId){
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(Options.getOptionsByUserid);
		query.setParameter("userid", userId);
	
		Options options = null;
		try{
				options = (Options) query.getSingleResult();
		}catch(NoResultException nre){
			System.out.println("No result exception occured");
		} 
		
		em.close();
		return options;
	}
	
}

