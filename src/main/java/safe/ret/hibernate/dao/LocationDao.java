package safe.ret.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import safe.ret.hibernate.HibernateUtil;
import safe.ret.hibernate.model.Location;

public class LocationDao extends AbstractDao{

	
	public Location createLocation(Location location) {
		return super.insert(location,Location.class);
	}

	
	public List<Location> getAllLocationsByTrackedUser(Long trackedUserID) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(Location.getAllLocationsByTrackedUser);
		query.setParameter("trackedUserID", trackedUserID);
		List<Location> locationList = query.getResultList();
		em.close();
		return locationList;
	}
	

	public List<Location> getLocationsOfTrackedUser(Long trackedUserID ,Boolean isSafe) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(Location.getLocationsByTrackedUserAndIsHome);
		query.setParameter("trackedUserID", trackedUserID);
		query.setParameter("home", isSafe);
		List<Location> location = query.getResultList();
		em.close();
		return location;
	}
	
	
	public Location getHomeLocationOfTrackedUser(Long trackedUserID) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(Location.getHomeLocationOfTrackedUser);
		query.setParameter("trackedUserID", trackedUserID);
		Location location = (Location) query.getSingleResult();
		em.close();
		return location;
	}
	
	
	public Location getLastKnownLocation(Long trackedUserId) {

		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(Location.getLastKnownLocation);
		query.setParameter("trackedUserID", trackedUserId);
		Location location = (Location) query.getSingleResult();
		em.close();
		return location;
	}
	
}
