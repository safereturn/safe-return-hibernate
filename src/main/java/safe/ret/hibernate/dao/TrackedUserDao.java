package safe.ret.hibernate.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Hibernate;

import safe.ret.hibernate.HibernateUtil;
import safe.ret.hibernate.model.Options;
import safe.ret.hibernate.model.TrackedUser;

public class TrackedUserDao extends AbstractDao{
	

	public TrackedUser saveOrUpdateTrackedUser(TrackedUser trackedUser) {
		
		return super.saveOrUpdate(trackedUser, TrackedUser.class, trackedUser.getId());
	}
	
	
	public TrackedUser getTrackedUserByID (Long id) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		TrackedUser trackedUser = em.find(TrackedUser.class, id);
		em.close();
		return trackedUser;
	}
	
	public TrackedUser getTrackedUserByIdPass(Long id, String pass) {
	
		TrackedUser trackedUser = null;
	
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(TrackedUser.getTrackedUserByIdPass);
		query.setParameter("id", id);
		query.setParameter("pass", pass);
		try{
			trackedUser = (TrackedUser) query.getSingleResult();
		}catch(NoResultException nre){
			System.out.println("No result exception occured");
		} 
		em.close();
		return trackedUser;
	}
	
	public List<TrackedUser> getAllTrackedUsers() {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		Query query = em.createNamedQuery(TrackedUser.getAllTrackedUsers);
		List<TrackedUser> trackedUserList = query.getResultList();
		em.close();
		return trackedUserList;
	}
 
	
 	public TrackedUser loadTrackedUserByID (Long id) {
		
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
//		EntityGraph graph = em.getEntityGraph(TrackedUser.WITH_CONTACTS);
//		Map hints = new HashMap();
//		hints.put("javax.persistence.fetchgraph", graph);
		TrackedUser trackedUser = em.find(TrackedUser.class, id );
		Hibernate.initialize(trackedUser.getTrackedContactAssoc());
		em.close();
		return trackedUser;
	}
 	
 	
	 public boolean deleteTrackedUser(Long id) {

		boolean result = false;
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		
		TrackedUser user = em.find(TrackedUser.class, id);
		if(user != null ){
			em.remove(user);
			result = true;
		}
		em.getTransaction().commit();
		em.close();

		return result;
	}
	 
	
	public int updateToken(Long userId, String token) {
		
		int rows;
		EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		Query query = em.createNamedQuery(TrackedUser.updateToken);
		query.setParameter("userId", userId);
		query.setParameter("token", token);
		rows = query.executeUpdate();
		em.getTransaction().commit();
		em.close();
		return rows;
	}
	 
	
}
