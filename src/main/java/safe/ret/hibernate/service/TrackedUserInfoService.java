//package safe.ret.hibernate.service;
//
//import safe.ret.hibernate.dao.TrackedUserInfoDao;
//import safe.ret.hibernate.model.TrackedUserInfo;
//
//public class TrackedUserInfoService {
//
//	private TrackedUserInfoDao dao = new TrackedUserInfoDao();
//	
//	public TrackedUserInfo getTrackedUserInfoByTrackedUser(Long trackedUserID) {
//		return this.dao.getTrackedUserInfoByTrackedUser(trackedUserID);
//	}
//	
//	public boolean saveOrUpdateTrackedUserInfo(TrackedUserInfo trackedUserInfo) {
//		return this.dao.saveOrUpdateTrackedUserInfo(trackedUserInfo);
//	}
//	
//	public boolean deleteTrackedUserInfo(Long trackedUserInfoID) {
//		return this.dao.deleteTrackedUserInfo(trackedUserInfoID);
//	}
//	
//}
