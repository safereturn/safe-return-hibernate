package safe.ret.hibernate.service;

import safe.ret.hibernate.dao.OptionsDao;
import safe.ret.hibernate.model.Options;

public class OptionsService {
	
	private OptionsDao dao = new OptionsDao();
	
	public Options getOptionsByUserId(Long trackedUserID) {
		return this.dao.getOptionsByUserid(trackedUserID);
	}

	public Options saveOrUpdateOptions(Options options) {
		return this.dao.saveOrUpdateOptions(options);
	}
	
	
//	public void saveOrUpdate...{
//		
//		//fetch option object for the tracked user 
//				Options opt =  getOptionsByUserId(options.getTrackedUser().getId());
//		 		if (opt!=null){
//		 			opt.setAllowedDistance(options.getAllowedDistance());
//		 			opt.setTimeInterval(options.getTimeInterval());
//		 			return this.dao.saveOrUpdateOptions(opt);
//		 		
//		 		}else{
//		 			//store as received,it's not an update
//		 			return this.dao.saveOrUpdateOptions(options);
//		 		}
//				
//			}
//
//	}

}
