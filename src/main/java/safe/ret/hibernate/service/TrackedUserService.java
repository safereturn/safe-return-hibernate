package safe.ret.hibernate.service;

import java.util.List;

import safe.ret.hibernate.dao.TrackedUserDao;
import safe.ret.hibernate.model.TrackedContactAssoc;
import safe.ret.hibernate.model.TrackedUser;

public class TrackedUserService {

	private TrackedUserDao dao = new TrackedUserDao();
	
	
	public List<TrackedUser> getAllTrackedUsers() {
		return this.dao.getAllTrackedUsers();
	}
	
	public TrackedUser getTrackedUserByID (Long id) {
		return this.dao.getTrackedUserByID(id);
	}

	public TrackedUser getTrackedUserByIdPass (Long id, String pass) {
		return this.dao.getTrackedUserByIdPass(id, pass);
	}
	
	public boolean deleteTrackedUser(Long id) {
		return this.dao.deleteTrackedUser(id);
	}

	public int updateToken(Long userId,String token){
		return this.dao.updateToken(userId,token);
	}
	 
	public TrackedUser saveOrUpdateTrackedUser(TrackedUser trackedUser) {
		return this.dao.saveOrUpdateTrackedUser(trackedUser);
	}
	
	// fetches contact persons also
	public TrackedUser loadTrackedUserById(Long id){
		return this.dao.loadTrackedUserByID(id);
	}

	public boolean hasContact(TrackedUser tu ,Long contactId) {
		// TODO Auto-generated method stub
		for(TrackedContactAssoc assoc : tu.getTrackedContactAssoc()){
			if (assoc.getContactPerson().getId() == contactId)
				return true;
		}
		return false;
	}

}
