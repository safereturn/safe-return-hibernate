package safe.ret.hibernate.service;

import java.util.List;

import safe.ret.hibernate.dao.ContactPersonDao;
import safe.ret.hibernate.model.ContactPerson;
import safe.ret.hibernate.model.TrackedUser;

public class ContactPersonService{
 

	private ContactPersonDao dao = new ContactPersonDao();
	
 
	public ContactPerson getContactPersonByID (Long id) {
		return this.dao.getContactPersonByID(id);
	}

	public List<ContactPerson> getContactsByTrackedUser(TrackedUser trackedUser) {
		return this.dao.getContactsByTrackedUser(trackedUser);
	}
	
	public ContactPerson saveOrUpdateContactPerson(ContactPerson contactPerson) {
		return this.dao.saveOrUpdateContactPerson(contactPerson);
	}
	
	public boolean deleteContactPerson(Long contactPersonID) {
		return this.dao.deleteContactPerson(contactPersonID);
	}
	
	public int updateToken(Long userId,String token){
		return this.dao.updateToken(userId,token);
	}
	
	public ContactPerson findByMobile(String mobile){
		return this.dao.findByMobile(mobile);
	}
 
}
