package safe.ret.hibernate.service;

import java.util.List;

import safe.ret.hibernate.dao.LocationDao;
import safe.ret.hibernate.model.Location;

public class LocationService {

	private LocationDao dao = new LocationDao();
	
 	
	public List<Location> getAllLocationsByTrackedUser(Long trackedUserID) {
		return this.dao.getAllLocationsByTrackedUser(trackedUserID);
	}
	
	public Location getHomeLocationOfTrackedUser(Long trackedUserID) {
		return this.dao.getHomeLocationOfTrackedUser(trackedUserID);
	}
	 
	public Location createLocation(Location location) {
		return this.dao.createLocation(location);
	}
	
 
	public Location getLastKnownLocation(Long trackedUserId){
		return this.dao.getLastKnownLocation(trackedUserId);
	}

	public List<Location> getLocationsOfTrackedUser(Long trackedUserId, Boolean isSafe) {
		return this.dao.getLocationsOfTrackedUser(trackedUserId,isSafe);
	}
	
	
}
